<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\CMS\MVC\Model\BaseDatabaseModel;
jimport('joomla.application.component.helper');

defined('_JEXEC') or die;

/**
 * Ff06_statistics model.
 *
 * @package  ff06_joomla_template
 * @since    1.0
 */
class Ff06_statisticsModelFf06_statistics extends BaseDatabaseModel {

    /**
     * @var $statisticsYear The year for which statistics are created. The year has to be set in the administrator menu
     * @since 1.0
     */
    protected $statisticsYear;

    protected $monthlyColorPalette = array('#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B');
    protected $typeColorPalette = array('#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B');
    protected $districtColorPalette = array('#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B');
    protected $vehicleColorPalette = array('#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B');
    protected $yearColorPalette = array('#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEF3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B');


    /**
     * Determines the year for which statistics are created
     *
     * @return int The statistics year
     * @throws Exception
     * @since 1.0
     */
    public function getStatisticsYear(){
        if(!isset($this->statisticsYear)){
            $jinput = JFactory::getApplication()->input;
            $this->statisticsYear = $jinput->get('statisticsyear', 1, 'INT');
        }
        return (int)$this->statisticsYear;
    }


    /**
     * Generates the canvas object and the javascript code for the statistics by month
     *
     * @return string The canvas object and javascript code
     * @since 1.0
     */
    public function getServiceByMonthStatistics() {
        try {
            $stats = '<canvas id="stat-service-per-month" style="width: 100%; height: 100%; display: block;"></canvas>';
            $stats .= $this->generateServiceByMonthJS(true);
            return $stats;
        } catch (Exception $e) {
            return '<p>The statistics module is unavailable at the moment</p>';
        }
    }


    /**
     * Generates the canvas object and the javascript code for the statistics by service type
     *
     * @return string The canvas object and javascript code
     * @since 1.0
     */
    public function getServiceByTypeStatistics() {
        try {
            $stats = '<canvas id="stat-service-per-type" style="width: 100%; height: 100%; display: block;"></canvas>';
            $stats .= $this->generateServiceByTypeJS(true);
            return $stats;
        } catch (Exception $e) {
            return '<p>The statistics module is unavailable at the moment</p>';
        }
    }


    /**
     * Generates the canvas object and the javascript code for the statistics by district
     *
     * @return string The canvas object and javascript code
     * @since 1.0
     */
    public function getServiceByDistrictStatistics() {
        try {
            $stats = '<canvas id="stat-service-per-district" style="width: 100%; height: 100%; display: block;"></canvas>';
            $stats .= $this->generateServiceByDistrictJS(true);
            return $stats;
        } catch (Exception $e) {
            return '<p>The statistics module is unavailable at the moment</p>';
        }
    }


    /**
     * Generates the canvas object and the javascript code for the statistics by district
     *
     * @return string The canvas object and javascript code
     * @since 1.0
     */
    public function getServiceByVehicleStatistics() {
        try {
            $stats = '<canvas id="stat-service-per-vehicle" style="width: 100%; height: 100%; display: block;"></canvas>';
            $stats .= $this->generateServiceByVehicleJS(true);
            return $stats;
        } catch (Exception $e) {
            return '<p>The statistics module is unavailable at the moment</p>';
        }
    }


    public function getServiceOverAllYearsStatistics() {
        try {
            $stats = '<canvas id="stat-service-all-years" style="width: 100%; height: 100%; display: block;"></canvas>';
            $stats .= $this->generateServiceOverAllYearsJS(true);
            return $stats;
        } catch (Exception $e) {
            return '<p>The statistics module is unavailable at the moment</p>';
        }
    }

    /**
     * Executes an SQL Query which fetches the statistics for a year from the database, grouped by month
     *
     * @return mixed An associative array
     * @throws Exception
     * @since 1.0
     * @todo Add order by clause
     */
    private function fetchServicesByMonthStatisticsFromDB() {

        $dbo = JFactory::getDbo();
        $squery = $dbo->getQuery(true);
        $squery->select(array('MONTH(date1) AS month', 'COUNT(*) AS count'))
            ->from($dbo->quoteName('#__eiko_einsatzberichte'))
            ->where('YEAR(date1) =' . $this->getStatisticsYear())
            ->group('MONTH(date1)');
        $dbo->setQuery($squery);

         // var_dump((string)$dbo->getQuery());

        return $dbo->loadAssocList();
    }


    /**
     * Executes an SQL Query which fetches the statistics for a year from the database, grouped by type
     *
     * @return mixed An associative array
     * @throws Exception
     * @since 1.0
     * @todo add order by clause
     */
    public function fetchServicesByTypeStatisticsFromDB() {

        $dbo = JFactory::getDbo();
        $tquery = $dbo->getQuery(true);
        $tquery->select(array('ea.title AS title', 'COUNT(*) AS count'))
            ->from($dbo->quoteName('#__eiko_einsatzberichte', 'eb'))
            ->join('INNER', $dbo->quoteName('#__eiko_einsatzarten', 'ea') . 'ON (' . $dbo->quoteName('eb.data1') . '=' . $dbo->quoteName('ea.id') .')')
            ->where('YEAR (date1) = ' . $this->getStatisticsYear())
            ->group('ea.title');
        $dbo->setQuery($tquery);

        return $dbo->loadAssocList();
    }


    /**
     * Executes an SQL Query which fetches the statistics for a year from the database, grouped by districts
     * The districts are stored in the #__eiko_alarmierungsarten table (abuse of previous database structure)
     *
     * @return mixed An associative array
     * @throws Exception
     * @since 1.0
     * @todo add order by clause
     */
    public function fetchServicesByDistrictStatisticsFromDB(){

        $dbo = JFactory::getDbo();
        $dquery = $dbo->getQuery(true);
        $dquery->select(array('d.title AS title', 'COUNT(*) AS count'))
            ->from($dbo->quoteName('#__eiko_einsatzberichte', 'eb'))
            ->join('INNER', $dbo->quoteName('#__eiko_alarmierungsarten', 'd') . 'ON (' . $dbo->quoteName('eb.alerting') . '=' . $dbo->quoteName('d.id') .')')
            ->where('YEAR (date1) = ' . $this->getStatisticsYear())
            ->group('d.title');
        $dbo->setQuery($dquery);

        return $dbo->loadAssocList();
    }


    public function fetchServicesByVehicleStatisticsFromDB(){
        $dbo = JFactory::getDbo();
        $vquery = $dbo->getQuery(true);
        $vquery->select(array('v.name AS name', 'COUNT(*) AS count'))
            ->from($dbo->quoteName('#__eiko_einsatzberichte', 'eb'))
            ->join('INNER', $dbo->quoteName('#__eiko_fahrzeuge', 'v') . 'ON FIND_IN_SET(v.id, eb.vehicles)')
            ->where('YEAR (date1) = ' . $this->getStatisticsYear())
            ->group('v.name');
        $dbo->setQuery($vquery);

        return $dbo->loadAssocList();
    }


    public function fetchServicesForAllYearsFromDB() {
        $dbo = JFactory::getDbo();
        $vquery = $dbo->getQuery(true);
        $vquery->select(array('YEAR(date1) as year', 'COUNT(*) AS count'))
            ->from($dbo->quoteName('#__eiko_einsatzberichte', 'eb'))
            ->group('YEAR(date1)');
        $dbo->setQuery($vquery);

        return $dbo->loadAssocList();
    }

    /**
     * Generates the JavaScript code for the services by month
     * The year is determined by the option of the component
     * @param bool $wrapScriptTag Flag if <script></script> shall be around the JS code
     * @return string The actual JavaScript Code
     * @throws Exception
     * @author Mark Lukas Möller
     * @since 1.0
     */
    private function generateServiceByMonthJS($wrapScriptTag = true) {

        $jscode = ($wrapScriptTag) ? "<script>" : "";

        $jscode .= $this->generateDataPerMonth();
        $jscode .= $this->generateOptionsForBarChart('barOptions');

        $jscode .= "var ctxPerMonth = document.getElementById(\"stat-service-per-month\").getContext('2d');";
        $jscode .= "var chartPerMonth = new Chart(ctxPerMonth, {type: 'bar', data: dataPerMonth, options: barOptions});";

        $jscode .= ($wrapScriptTag) ? "</script>" : "";

        return $jscode;
    }


    /**
     * Generates the JavaScript code for the services by type
     * The year is determined by the option of the component
     * @param bool $wrapScriptTag Flag if <script></script> shall be around the JS code
     * @return string The actual JavaScript Code
     * @throws Exception
     * @author Mark Lukas Möller
     * @since 1.0
     */
    private function generateServiceByTypeJS($wrapScriptTag = true) {

        $jscode = ($wrapScriptTag) ? "<script>" : "";

        $jscode .= $this->generateDataPerType();
        $jscode .= $this->generateOptionsForPieChart('pieOptions');

        $jscode .= "var ctxPerType = document.getElementById(\"stat-service-per-type\").getContext('2d');";
        $jscode .= "var chartPerType = new Chart(ctxPerType, {type: 'pie', data: dataPerType, options: pieOptions});";

        $jscode .= ($wrapScriptTag) ? "</script>" : "";

        return $jscode;
    }


    private function generateServiceByDistrictJS($wrapScriptTag = true) {
        $jscode = ($wrapScriptTag) ? "<script>" : "";

        $jscode .= $this->generateDataPerDistrict();
        $jscode .= $this->generateOptionsForPieChart('pieOptions');

        $jscode .= "var ctxPerDistrict = document.getElementById(\"stat-service-per-district\").getContext('2d');";
        $jscode .= "var chartPerDistrict = new Chart(ctxPerDistrict, {type: 'pie', data: dataPerDistrict, options: pieOptions});";

        $jscode .= ($wrapScriptTag) ? "</script>" : "";

        return $jscode;
    }


    private function generateServiceByVehicleJS($wrapScriptTag = true) {
        $jscode = ($wrapScriptTag) ? "<script>" : "";

        $jscode .= $this->generateDataPerVehicle();
        $jscode .= $this->generateOptionsForBarChart('barOptions');

        $jscode .= "var ctxPerVehicle = document.getElementById(\"stat-service-per-vehicle\").getContext('2d');";
        $jscode .= "var chartPerVehicle = new Chart(ctxPerVehicle, {type: 'bar', data: dataPerVehicle, options: barOptions});";

        $jscode .= ($wrapScriptTag) ? "</script>" : "";

        return $jscode;
    }


    private function generateServiceOverAllYearsJS($wrapScriptTag = true) {
        $jscode = ($wrapScriptTag) ? "<script>" : "";

        $jscode .= $this->generateDataOverAllYears();
        $jscode .= $this->generateOptionsForLineChart('lineOptions');

        $jscode .= "var ctxAllYears = document.getElementById(\"stat-service-all-years\").getContext('2d');";
        $jscode .= "var chartAllYears = new Chart(ctxAllYears, {type: 'line', data: dataAllYears, options: lineOptions});";

        $jscode .= ($wrapScriptTag) ? "</script>" : "";

        return $jscode;
    }

    /**
     * Generates the JSON data object for the data per month statistics (and everything beyond in the AST)
     *
     * @return string The JSON data object
     * @throws Exception
     * @since 1.0
     * @todo Simplify to remove first foreach loop
     */
    private function generateDataPerMonth() {

        $labels = array();
        $labelsJS = '';
        $data = array();
        $dataJS = '';
        $bgcolor = array();
        $bgcolorJS = '';

        $dataToProcess = $this->fetchServicesByMonthStatisticsFromDB();
        foreach ($dataToProcess as $row) {

            /*
             * Append month label array
             */

            switch ($row['month']) {
                case 1: array_push($labels, 'Januar'); break;
                case 2: array_push($labels, 'Februar'); break;
                case 3: array_push($labels, 'März'); break;
                case 4: array_push($labels, 'April'); break;
                case 5: array_push($labels, 'Mai'); break;
                case 6: array_push($labels, 'Juni'); break;
                case 7: array_push($labels, 'Juli'); break;
                case 8: array_push($labels, 'August'); break;
                case 9: array_push($labels, 'September'); break;
                case 10: array_push($labels, 'Oktober'); break;
                case 11: array_push($labels, 'November'); break;
                case 12: array_push($labels, 'Dezember'); break;
            }

            array_push($data, $row['count']);
            array_push($bgcolor, $this->monthlyColorPalette[(int)($row['month']-1)]);
        }

        /*
         * Generate Javascript objects bottom-up
         */

        foreach ($labels as $label) {
            $labelsJS .= '"' . $label . '", ';
        }

        $labelsJS = 'labels: ['. $labelsJS . '],';

        foreach ($data as $singledata) {
            $dataJS .= $singledata .', ';
        }

        $dataJS = 'data: [' . $dataJS .'],';

        foreach ($bgcolor as $monthlycolor) {
            $bgcolorJS .= '\'' . $monthlycolor .'\', ';
        }

        $bgcolorJS = 'backgroundColor: [' . $bgcolorJS . '], ';
        $datasetsJS = 'datasets: [{' . $dataJS . $bgcolorJS . '}], ';
        $payloadDataObject = ' dataPerMonth = {' . $labelsJS . $datasetsJS . '};';

        return $payloadDataObject;
    }


    /**
     * Generates the JSON data object for the data per type statistics (and everything beyond in the AST)
     *
     * @return string The JSON data object
     * @throws Exception
     * @since 1.0
     * @todo Simplify to remove first foreach loop
     */
    private function generateDataPerType() {

        $labels = array();
        $labelsJS = '';
        $data = array();
        $dataJS = '';
        $bgcolor = array();
        $bgcolorJS = '';

        $dataToProcess = $this->fetchServicesByTypeStatisticsFromDB();

        foreach ($dataToProcess as $key => $row) {
            array_push($labels, $row['title']);
            array_push($data, $row['count']);
            array_push($bgcolor, $this->typeColorPalette[(int)$key]);
        }

        /*
         * Generate Javascript objects bottom-up
         */

        foreach ($labels as $label) {
            $labelsJS .= '"' . $label . '", ';
        }

        $labelsJS = 'labels: ['. $labelsJS . '],';

        foreach ($data as $singledata) {
            $dataJS .= $singledata .', ';
        }

        $dataJS = 'data: [' . $dataJS .'],';

        foreach ($bgcolor as $monthlycolor) {
            $bgcolorJS .= '\'' . $monthlycolor .'\', ';
        }

        $bgcolorJS = 'backgroundColor: [' . $bgcolorJS . '], ';
        $datasetsJS = 'datasets: [{' . $dataJS . $bgcolorJS . '}], ';
        $payloadDataObject = ' dataPerType = {' . $labelsJS . $datasetsJS . '};';

        return $payloadDataObject;
    }


    public function generateDataPerDistrict() {
        $labels = array();
        $labelsJS = '';
        $data = array();
        $dataJS = '';
        $bgcolor = array();
        $bgcolorJS = '';

        $dataToProcess = $this->fetchServicesByDistrictStatisticsFromDB();

        foreach ($dataToProcess as $key => $row) {
            array_push($labels, $row['title']);
            array_push($data, $row['count']);
            array_push($bgcolor, $this->typeColorPalette[(int)$key % 12]);
        }

        /*
         * Generate Javascript objects bottom-up
         */

        foreach ($labels as $label) {
            $labelsJS .= '"' . $label . '", ';
        }

        $labelsJS = 'labels: ['. $labelsJS . '],';

        foreach ($data as $singledata) {
            $dataJS .= $singledata .', ';
        }

        $dataJS = 'data: [' . $dataJS .'],';

        foreach ($bgcolor as $monthlycolor) {
            $bgcolorJS .= '\'' . $monthlycolor .'\', ';
        }

        $bgcolorJS = 'backgroundColor: [' . $bgcolorJS . '], ';
        $datasetsJS = 'datasets: [{' . $dataJS . $bgcolorJS . '}], ';
        $payloadDataObject = ' dataPerDistrict = {' . $labelsJS . $datasetsJS . '};';

        return $payloadDataObject;
    }


    public function generateDataPerVehicle() {
        $labels = array();
        $labelsJS = '';
        $data = array();
        $dataJS = '';
        $bgcolor = array();
        $bgcolorJS = '';

        $dataToProcess = $this->fetchServicesByVehicleStatisticsFromDB();

        foreach ($dataToProcess as $key => $row) {
            array_push($labels, $row['name']);
            array_push($data, $row['count']);
            array_push($bgcolor, $this->vehicleColorPalette[(int)$key % 12]);
        }

        /*
         * Generate Javascript objects bottom-up
         */

        foreach ($labels as $label) {
            $labelsJS .= '"' . $label . '", ';
        }

        $labelsJS = 'labels: ['. $labelsJS . '],';

        foreach ($data as $singledata) {
            $dataJS .= $singledata .', ';
        }

        $dataJS = 'data: [' . $dataJS .'],';

        foreach ($bgcolor as $monthlycolor) {
            $bgcolorJS .= '\'' . $monthlycolor .'\', ';
        }

        $bgcolorJS = 'backgroundColor: [' . $bgcolorJS . '], ';
        $datasetsJS = 'datasets: [{' . $dataJS . $bgcolorJS . '}], ';
        $payloadDataObject = ' dataPerVehicle = {' . $labelsJS . $datasetsJS . '};';

        return $payloadDataObject;
    }


    private function generateDataOverAllYears() {
        $labels = array();
        $labelsJS = '';
        $data = array();
        $dataJS = '';
        $linecolor = array();
        $linecolorJS = '';

        $dataToProcess = $this->fetchServicesForAllYearsFromDB();

        foreach ($dataToProcess as $key => $row) {
            array_push($labels, $row['year']);
            array_push($data, $row['count']);
            array_push($linecolor, $this->yearColorPalette[0]);
        }

        /*
         * Generate Javascript objects bottom-up
         */

        foreach ($labels as $label) {
            $labelsJS .= '"' . $label . '", ';
        }

        $labelsJS = 'labels: ['. $labelsJS . '],';

        foreach ($data as $singledata) {
            $dataJS .= $singledata .', ';
        }

        $dataJS = 'data: [' . $dataJS .'],';

        /*
        foreach ($linecolor as $yearcolor) {
            $linecolorJS .= '\'' . $yearcolor .'\', ';
        }
        */

        $linecolorJS = '\'' . $linecolor[0] .'\', ';

        $linecolorJS = 'backgroundColor: [' . $linecolorJS . '], borderColor: [' . $linecolorJS . '], fill: false, label: \'Einsätze insgesamt\', ';
        $datasetsJS = 'datasets: [{' . $dataJS . $linecolorJS . '}], ';
        $payloadDataObject = ' dataAllYears = {' . $labelsJS . $datasetsJS . '};';

        return $payloadDataObject;
    }

    /**
     * Generates the option object for the Javascript code
     * Currently this is static. Hence, this is just a String bilder
     *
     * @param $optname The name of the options object
     * @return string The options object
     * @since 1.0
     */
    private function generateOptionsForPieChart($optname) {

        return ' ' . $optname .'= { legend: { position: \'left\', labels: { boxWidth: 12, usePointStyle: true } }, responsive: true, maintainAspectRatio: false };';
    }


    private function generateOptionsForBarChart($optname) {

        return ' ' . $optname .'= { legend: {display: false}, scales: { xAxes: [{ ticks: {autoSkip: false}, gridLines: {display: false} }], yAxes: [{ ticks: {beginAtZero: true} }] }, responsive: true, maintainAspectRatio: false };';
    }

    private function generateOptionsForLineChart($optname) {

        return ' ' . $optname .'= {legend: { labels: { boxWidth: 12, usePointStyle: true } }, scales: {xAxes: [{display: true, scaleLabel: {display: true, labelString: \'Jahr\'} }], yAxes: [{display: true, scaleLabel: {display: true, labelString: \'Anzahl Einsätze\'}, ticks: {beginAtZero: true} }]}, responsive: true, maintainAspectRatio: false };';

    }
}
