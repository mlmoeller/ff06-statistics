<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\MVC\View\HtmlView;

defined('_JEXEC') or die;

/**
 * Ff06_statistics view.
 *
 * @package  ff06_joomla_template
 * @since    1.0
 */
class Ff06_statisticsViewFf06_statistics extends HtmlView
{
    function display($tpl = null)
    {
        $this->statisticsYear = $this->get('StatisticsYear');
        parent::display($tpl);
    }
}
