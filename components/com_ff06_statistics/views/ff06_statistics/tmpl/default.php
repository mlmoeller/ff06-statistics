<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Layout\FileLayout;

defined('_JEXEC') or die;

HTMLHelper::_('script', 'com_ff06_statistics/Chart.bundle.min.js', array('version' => 'auto', 'relative' => true));
HTMLHelper::_('script', 'com_ff06_statistics/Chart.util.js', array('version' => 'auto', 'relative' => true));

$layout = new FileLayout('ff06_statistics.page');

$statisticsyear = $this->get('StatisticsYear');

?>

<section class="page-title">
    <div class="container">
        <ul class="breadcrumb">
            <?php
            $breadcrumb_objects = JFactory::getApplication()->getPathWay()->getPathway();
            foreach ($breadcrumb_objects as $key => $bc_obj) {
                if(count($breadcrumb_objects)-1 != $key) {
                    echo '<li><a target="_self" href="' . JURI::base(true) . '/' . $bc_obj->link . '">' . $bc_obj->name . '</a></li>';
                } else {
                    echo '<li class="active">' . $bc_obj->name . '</li>';
                }
            }
            ?>
        </ul>
        <header>
            <h1>Einsatzstatistik <?php echo $statisticsyear ?></h1>
        </header>
    </div>
</section>
<section>
    <div class="container">
        <?php if ($statisticsyear == date("Y")) : ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">
                        <strong>Hinweis: </strong> Diese Statistik bezieht sich auf die bisher eingetragenen Einsätze des laufenden Kalenderjahres. Die Statistik ist daher noch nicht abgeschlossen. Entsprechend ist es möglich, dass einige Fahrzeuge, Einsatzarten oder Stadtteile noch nicht in der Statistik auftauchen!
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6">
                <div style="padding-left:25px; padding-right: 25px; margin-bottom: 100px;">
                    <div style="height:300px">
                        <h2>Einsätze nach Monaten</h2>
                        <?php echo $this->get('ServiceByMonthStatistics'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div style="padding-left:25px; padding-right: 25px; margin-bottom: 100px;">
                    <div style="height:300px">
                        <h2>Einsätze nach Einsatzart</h2>
                        <?php echo $this->get('ServiceByTypeStatistics'); ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div style="padding-left:25px; padding-right: 25px; margin-bottom: 100px;">
                    <div style="height:300px">
                        <h2>Einsätze nach Stadtteil</h2>
                        <?php echo $this->get('ServiceByDistrictStatistics'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div style="padding-left:25px; padding-right: 25px; margin-bottom: 100px;">
                    <div style="height:300px">
                        <h2>Einsätze nach Fahrzeug</h2>
                        <?php echo $this->get('ServiceByVehicleStatistics'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-title">
    <div class="container">
        <header>
            <h1>Statistiken über alle Jahre</h1>
        </header>
    </div>
</section>
<section>
    <div class="container">
        <!--
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-default">
                    <strong>Hinweis: </strong> Die Statistik nach Einsatzarten wird wie folgt erstellt:<br />
                    Zu <strong>Brandbekämpfung </strong> werden folgende Einsatzarten zusammengefasst: Brandeinsatz groß, Brandeinsatz mittel, Brandeinsatz klein, Brandmeldeanlage, Rauchwarnmelder.<br />
                    Zu <strong>Technische Hilfeleistung</strong> werden folgende Einsatzarten zusammengefasst: Technische Hilfeleistung, Techn. Hilfeleistung - Menschenleben in Gefahr, CBRN-Einsatz, Hilfeleistung auf Gewässer / Wasserrettung.
                </div>
            </div>
        </div>
        -->
        <div class="row">
            <div class="col-md-12">
                <div style="height: 300px">
                    <?php echo $this->get('ServiceOverAllYearsStatistics') ?>
                </div>
            </div>
        </div>
    </div>
</section>