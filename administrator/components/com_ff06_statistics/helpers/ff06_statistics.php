<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\Language\Text;

defined('_JEXEC') or die;

/**
 * Ff06_statistics helper.
 *
 * @package     A package name
 * @since       1.0
 */
class Ff06_statisticsHelper
{
	/**
	 * Render submenu.
	 *
	 * @param   string  $vName  The name of the current view.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(Text::_('COM_FF06_STATISTICS'), 'index.php?option=com_ff06_statistics&view=ff06_statistics', $vName == 'ff06_statistics');
	}
}
