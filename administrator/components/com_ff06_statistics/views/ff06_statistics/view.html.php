<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView;

defined('_JEXEC') or die;

/**
 * Ff06_statistics view.
 *
 * @package  ff06_joomla_template
 * @since    1.0
 */
class Ff06_statisticsViewFf06_statistics extends HtmlView
{
	/**
	 * Ff06_statistics helper
	 *
	 * @var    Ff06_statisticsHelper
	 * @since  1.0
	 */
	protected $helper;

	/**
	 * The sidebar to show
	 *
	 * @var    string
	 * @since  1.0
	 */
	protected $sidebar = '';

	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a JError object.
	 *
	 * @see     fetch()
	 * @since   1.0
	 */
	public function display($tpl = null)
	{
		// Show the toolbar
		$this->toolbar();

		// Show the sidebar
		$this->helper = new Ff06_statisticsHelper;
		$this->helper->addSubmenu('ff06_statistics');
		$this->sidebar = JHtmlSidebar::render();

		// Display it all
		return parent::display($tpl);
	}

	/**
	 * Displays a toolbar for a specific page.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	private function toolbar()
	{
		JToolBarHelper::title(Text::_('COM_FF06_STATISTICS'), '');

		// Options button.
		if (Factory::getUser()->authorise('core.admin', 'com_ff06_statistics'))
		{
			JToolBarHelper::preferences('com_ff06_statistics');
		}
	}
}
