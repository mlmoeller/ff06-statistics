<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

defined('_JEXEC') or die;

?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <h2>FF Stadt-Mitte Einsatzstatistik</h2>
    <p>Diese Komponente verfügt über keine Konfigurationsoptionen</p>
    <p>Um eine Einsatzstatistik zu generieren, erstellen sie ein Menüitem, wählen Einsatzstatistik aus und stellen die entsprechende Jahreszahl ein.</p>
</div>
