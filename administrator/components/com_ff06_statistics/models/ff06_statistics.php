<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\MVC\Model\ListModel;

defined('_JEXEC') or die;

/**
 * Ff06_statistics
 *
 * @package  ff06_joomla_template
 * @since    1.0
 */
class Ff06_statisticsModelFf06_statistics extends ListModel
{
}
