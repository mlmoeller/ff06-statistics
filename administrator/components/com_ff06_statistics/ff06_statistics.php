<?php
/**
 * @package    ff06_joomla_template
 *
 * @author     marklukasmoller <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\BaseController;

defined('_JEXEC') or die;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_ff06_statistics'))
{
	throw new InvalidArgumentException(Text::_('JERROR_ALERTNOAUTHOR'), 404);
}

// Require the helper
require_once JPATH_COMPONENT_ADMINISTRATOR . '/helpers/ff06_statistics.php';

// Execute the task
$controller = BaseController::getInstance('ff06_statistics');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
